from . import Animal


class Chien(Animal):

    def __init__(self, nom):
        print("Chien: ", nom)
        super().__init__("Chien", nom, 4)

    def se_deplacer(self, x, y):
        self.position.x += x
        self.position.y += y
