#!/usr/bin/python
# -*- coding: utf-8 -*-

from .Position import Position


class Animal:

    def __init__(self, espece, nom, nb_pattes):
        print("Animal ", nom)
        self.is_animal = True
        self.espece = espece
        self.nom = nom
        self.nb_pattes = nb_pattes
        self.is_hungy = True
        self.position = Position(0, 0)

    def manger(self):
        self.is_hungy = False

    def se_deplacer(self, x, y):
        self.position.x = x
        self.position.y = y

    def __str__(self):
        return "Hello"
        # return f"Je suis un {self.espece}, j'ai {self.nb_pattes} pattes, et je m'appelle {self.nom}"
