from . import Animal


class Chat(Animal):

    def __init__(self, nom):
        print("Chat ", nom)
        super().__init__("Chat", nom, 4)
        self.is_sleeping=False

    def manger(self):
        print("Je mange de la pate")

    def sleep(self):
        self.is_sleeping=True
