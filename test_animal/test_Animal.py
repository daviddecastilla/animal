from animal import Animal


def test_init():
    animal = Animal("Crabe", "Capitaine", 8)

    assert animal.espece == "Crabe"
    assert animal.nom == "Capitaine"
    assert animal.is_animal
    assert animal.nb_pattes == 8
