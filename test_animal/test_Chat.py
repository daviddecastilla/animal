from animal.Chat import Chat


def test_init():
    chat = Chat("Gladys")

    assert chat.nom == "Gladys"
    assert chat.nb_pattes == 4
    assert chat.espece == "Chat"
    assert chat.is_animal
    assert chat.position.x == 0
    assert chat.position.y == 0
    assert not chat.is_sleeping


def test_se_deplacer():
    chat=Chat("Gladys")
    chat.se_deplacer(6, 9)
    assert chat.position.x == 6
    assert chat.position.y == 9


def test_sleep():
    chat = Chat("Gladys")
    chat.sleep()
    assert chat.is_sleeping
